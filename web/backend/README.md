# Getting started for the Raspberry Pi

## Install Node.js

    wget https://nodejs.org/dist/v12.18.4/node-v12.18.4-linux-armv7l.tar.xz

    tar -xf node-v12.18.4-linux-armv7l.tar.xz

    cd node-v12.18.4-linux-armv7l/
    sudo cp -R * /usr/local/

## Clone this repo

    git clone https://gitlab.com/ANODIN/beehive.git

## Configuration

Change SET_OUR_PRIVATE_KEY_HERE and SET_OUR_DATABASE_NAME by you own information

    cd beehive/backend/cfg
    cp ./config.default.json ./config.json
    nano ./config.json

## Run the server

    cd ..
    npm i
    npm start
