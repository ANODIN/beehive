const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const logger = require('morgan');
const utils = require('./app/middleware/utils');

const defaultConfig = require('./cfg/config.json').static;

/**
 * Import Router
 */
const viewRouter = require('./routes/view/viewRoute');
const userRouter = require('./routes/api/userRoute');
const beehiveRouter = require('./routes/api/beehiveRoute');
const weatherRouter = require('./routes/api/weatherRouter');

var app = express();

/**
 * Init db connection and create API routes
 */
mongoose.connect(defaultConfig.database.url, defaultConfig.database.option)
    .then(() => console.log('[MONGOOSE] Connexion à MongoDB réussie !'))
    .catch(e => console.log('[MONGOOSE] Connexion à MongoDB échouée :', e.message));

/**
 * View engine setup
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

/**
 * Midlewares
 */
app.use(utils.cors);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Router
 */
app.use('/', viewRouter);
app.use('/api/beehive', beehiveRouter);
app.use('/api/user', userRouter);
app.use('/api/ws', weatherRouter);

/**
 * Catch 404 and forward to error handler
 */
// app.use(function(req, res, next) {
//   next(createError(404));
// });

/**
 * error handler
 */
app.use((err, req, res, next)  => {
  // set locals, only providing error in development
  // res.locals.message = err.message;
  // res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({msg : err.message});
  // res.render('error');
});

module.exports = app;
