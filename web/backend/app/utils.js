/**
 * Extract values that match the schema
 * @param input array
 * @param pattern array
 * @returns {{array}}
 */
exports.extractValue = (input, pattern) => {
    if(!input || !pattern)
        return undefined;
    if(!Array.isArray(pattern))
        return undefined;

    const r = {}
    Object.keys(input).forEach(k => {
        let value = input[k];
        switch (typeof value){
            case "boolean":
            case "number":
            case "string":
            case "object":
                if (pattern.indexOf(k) > -1)
                    r[k] = value;
                break;
        }
    });
    return r;
}