const mongoose = require ('mongoose');

const configurationSchema = require('../Configuration').schema;
const coordinateSchema = require('../Coordinate').schema;
const measureArraySchema = require('./WeatherStationArray').schema;
const userSchema = require('../User').schema;

const _schema = mongoose.Schema({
   ownerId: {type: String, require: true },
   name: {type: String, default: ''},
   desc: {type: String, default: ''},
   coordinate: {type: coordinateSchema, default: coordinateSchema},
   configuration: {type: configurationSchema, default: configurationSchema, require: true},
   measures: [measureArraySchema],
   lastConnexion: {type: Date, default: Date.now},
});

exports.schema = _schema;
exports.model = mongoose.model('WeatherStation', _schema);
