const mongoose = require ('mongoose');

const _schema = mongoose.Schema({
    humidity: {type: Number, default: 0},
    temperature: {type: Number, default: 0},
    windDirection: {type: String, default: ''},
    windSpeed: {type: Number, default: 0},
    rainfall : {type: Number, default: 0},
    sunshine: {type: Number, default: 0}
});

exports.schema = _schema;
exports.model = mongoose.model('WeatherStationMeasure', _schema);
