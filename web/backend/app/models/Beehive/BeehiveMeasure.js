const mongoose = require('mongoose');

const _schema = mongoose.Schema({
    humidity: {type: Number, default: 0},
    temperature: {type: Number, default: 0},
    weight: {type: Number, default: 0},
    pressure: {type: Number, default: 0}
});

exports.schema = _schema;
exports.model = mongoose.model('BeehiveMeasure', _schema);
