const mongoose = require('mongoose');
const BeehiveMeasure = require('./BeehiveMeasure');

const _schema = mongoose.Schema({
    timestamp: {type: Date, default: Date.now},
    data: {type: BeehiveMeasure.schema, default: BeehiveMeasure.schema},
});

exports.schema = _schema;
exports.model = mongoose.model('BeehiveArray', _schema);
