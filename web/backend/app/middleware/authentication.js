const jwt = require('jsonwebtoken');
const createError = require('http-errors');

const defaultConfig = require('../../cfg/config.json').static;
/**
 * Check if request contain GUID
 * @param req
 * @param res
 * @param next
 */
exports.needGUID = (req, res, next) => {
    const {guid} = req.query;

    if(guid === undefined){
        req.quid = guid;
        res.status(400).send('Not authorized !');
    } else {
        next();
    }
}

/**
 * Check if password and email is correct
 * @param req
 * @param res
 * @param next
 */
exports.checkIdentity = (req, res, next) =>{
    let { password, email} = req.body;

    if(!password || !email)
        return next(createError(400, 'values are missing'))

    // Check password and email length
    password = password.trim();
    email = email.trim();
    if (password.length === 0 || email.length === 0)
        next(createError(400, 'username or password is invalid'));

    req.body = {
        password: password,
        email: email,
    }
    next();
}

/**
 * Check if token exist and deffiche the content
 * @param req
 * @returns {{userId: *}|undefined}
 * @private
 */
function _getToken({headers}){
    let token = ((headers.authorization)
        ? (headers.authorization.split(' ')[1] || undefined)
        : undefined);
    if(token){
        try{
            const decodedToken = jwt.verify(
                token,
                defaultConfig.privateKey);
            return {userId: decodedToken.userId};
        }catch (e){
            console.log('jwt error -->', e.message);
        }
    }

    return undefined;
}

/**
 * Just add token to the request (even if it does not exist)
 * @param req
 * @param res
 * @param next
 */
exports.getToken = (req, res, next) => {
    req.credential = _getToken(req);
    next();
}

/**
 * Decrypt the token, and retrieve user id then the stock in the body
 * @param req
 * @param res
 * @param next
 */
exports.needToken = (req, res, next) => {
    const credential = req.credential = _getToken(req);
    if(!credential)
        return next(createError(500, 'This request need a token !'));

    next();
};

/**
 * Generate a unique token for auth the user request
 * (use HS256 algorithm)
 * @param user
 * @returns {{userId: string, token: string}}
 */
exports.createToken = user => {
    return {
        userId: user._id,
        token: jwt.sign(
            {userId: user._id},
            defaultConfig.privateKey,
            {expiresIn: '24h'}),
    }
}