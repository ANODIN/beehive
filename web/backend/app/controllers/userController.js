const createError = require('http-errors')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const getCredential = require('../middleware/authentication').createToken;

const UserModel = require('../models/User').model;


/**
 * Create a new user account (require email and password)
 * and return an authentication token for the next communication
 * @param req
 * @param res
 * @param next
 */
exports.signup = (req, res, next) => {
    let { password, email} = req.body;

    // Generate user token
    bcrypt.hash(password, 10)
        .then(hash => {
            const newUser = new UserModel({
                email: email,
                password: hash
            });

            // Save user and return token
            newUser.save()
                .then(data => res.status(201).json(getCredential(data)))
                .catch(e => {
                    console.log(e.message);
                    next(createError(500, 'user has not been created'));
                });
        })
        .catch(e => next(createError(500)));
};

/**
 * Authenticated an user with email and password,
 * and return an authentication token for the next communication
 * @param req
 * @param res
 * @param next
 */
exports.login = (req, res, next) => {
    let { password, email} = req.body;

    UserModel.findOne({ email: email })
        .then(user => {
            if (!user)
                return next(createError(400, 'user does not exist'));

            bcrypt.compare(password, user.password)
                .then(valid => {
                    if (valid) {
                        res.status(200).json(getCredential(user));
                    }
                    else {
                        return next(createError(400, 'invalid password'));
                    }
                })
                .catch(e => next(createError(500)));
        })
        .catch(e => next(createError(500)));
};
