const createError = require('http-errors')
const extractValue = require('../../app/utils').extractValue;
const defaultConfig = require('../../cfg/config.json').static;

/**
 * Imports mongoDb models
 */
const CoordinateModel = require('../models/Coordinate').model;
const ConfigurationModel = require('../models/Configuration').model;
const WeatherStationModel = require('../models/Weather/WeatherStation').model;
const WeatherStationArrayModel = require('../models/Weather/WeatherStationArray').model;
const WeatherStationMeasureModel = require('../models/Weather/WeatherStationMeasure').model;


/**
 * Return date of the weather station
 * @param req
 * @param res
 * @param next
 */
exports.getData = (req, res, next) => {
    const {userId} = req.credential;

    WeatherStationModel.findOne({ownerId: userId}, {measures: 0}).then(data => {
        res.status(200).json(extractValue(data._doc, ['_id', 'desc', 'name', 'ownerId', 'configuration', 'coordinate', 'lastConnexion']));
    }).catch(e => next(400));
}


/**
 * Create a new configuration
 * @param req
 * @param res
 * @param next
 */
exports.createData = (req, res, next) => {
    const {userId} = req.credential;

    let data = extractValue(
        req.body || undefined,
        ['name', 'desc', 'coordinate', 'configuration']);
    if (!data || Object.keys(data).length === 0)
        return next(createError(400, 'body is incomplete or empty'));

    // Set coordinate
    if ('coordinate' in data && typeof (data['coordinate']) === "object") {
        let coordinateData = extractValue(data['coordinate'], ['lat', 'long']);
        if (coordinateData && Object.keys(coordinateData).length > 0)
            data['coordinate'] = new CoordinateModel({...coordinateData});
        else
            data['coordinate'] = new CoordinateModel();
    } else {
        data['coordinate'] = new CoordinateModel();
    }

    // Set configuration
    data['configuration'] = new ConfigurationModel({hasGPSModule: defaultConfig.hasGPS || false, isConfigured: true});

    // Set ownerId
    data['ownerId'] = userId;

    // Save the new data
    let newData = new WeatherStationModel(data);
    newData.save()
        .then(d => res.status(201).json({id: d._id, ...data}))
        .catch(e => next(createError(500)));
}

/**
 * Returns the measurements, an interval can be indicated
 * query : "from", timestamp of start of interval
 *         "to", timestamp of end of interval
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
exports.getMeasures = (req, res, next) => {
    const {userId} = req.credential;
    let {from, to} = extractValue(req.query, ['from', 'to']);

    // Check "from" date
    if (isNaN(from) || !from)
        return next(createError(400, 'from is not correct or absent'))
    else
        from = new Date(parseInt(from));

    // Check "to" date
    if (isNaN(to) || !to)
        to = new Date(Date.now());
    else
        to = new Date(parseInt(to) || Date.now());

    if(from > to)
        return next(createError(400, 'the future cannot be ahead of the present'))

    WeatherStationModel.findOne(
        {
            ownerId: userId,
            measures: {
                $elemMatch: {
                    // TODO : - filter not work, timezone problem ?
                    timestamp: {$gte: new Date(from.toUTCString())},
                }
            }
        }, {measures: 1})
        .then(data => {
            if (data){
                const measures = data.measures || undefined;
                if(measures)
                    console.log(measures.length)
                    // console.log(measures.forEach(d => {
                    //     console.log(new Date(d.timestamp).toUTCString())
                    //     console.log(from.getTime() <= new Date(d.timestamp).getTime());
                    // }))
            }
        })
        .catch(e => {console.log('error', e)});
    return next(createError(500))
}

/**
 * Add new measurement in the station
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
exports.setMeasures = (req, res, next) => {
    const {userId} = req.credential;
    const query = extractValue(req.query, ['humidity', 'temperature', 'windDirection', 'windSpeed', 'rainfall', 'sunshine']);

    if (Object.keys(query).length === 0)
        return next(createError(400));

    WeatherStationModel.updateOne(
        {ownerId: userId},
        {
            $push: {
                measures: new WeatherStationArrayModel({
                    data: new WeatherStationMeasureModel(query)
                })
            },
            $set: { lastConnexion: Date.now() }
        })
        .then(() => next(createError(200)))
        .catch(e => next(createError(400, e.message)));
}
