const createError = require('http-errors')
const extractValue = require('../../app/utils').extractValue;

const BeehiveModel= require('../models/Beehive/Beehive').model;
const BeehiveMeasure = require('../models/Beehive/BeehiveMeasure').model;
const BeehiveArrayModel = require('../models/Beehive/BeehiveArray').model;

exports.needGUID = (req, res, next) => {
    let data = extractValue(req.query, ['guid']);
    if('guid' in data){
        req.beehive = data;
        next();
    }else {
        next(createError(400, 'guid is empty or not correct'));
    }
}

/**
 * This function is called when a new beehive connect to the station
 * @param req
 * @param res
 * @param next
 */
exports.connect = (req, res, next) => {
    const { guid } = req.beehive;

    var communData = { 'guid': guid };
    BeehiveModel.findOneAndUpdate(communData, {'lastConnexion': Date.now()})
        .then(d => {
            if(d){
                res.status(200).json(extractValue(d._doc, ['guid', '_id', 'isAuthenticated']));
            }else {
                const newData = new BeehiveModel(communData);
                newData.save()
                    .then(d => res.status(200).json(extractValue(d._doc, ['guid', '_id', 'isAuthenticated'])))
                    .catch(e => next(createError(500, '??? ' + e.message)));
            }
        })
        .catch(e => next(createError(400, e.message)));
}

exports.get = (req, res, next) => {
    const credential = req.beehive || undefined;

    if(credential){
        BeehiveModel.findOne({'guid': credential['guid']}, {'measures': 0})
            .then(data => {
                res.status(200).json(data);
            })
            .catch(e => next(createError(500, e.message)));
    }else {
        BeehiveModel.find({}, {'measures': 0})
            .then(data => {
                res.status(200).json(data);
            })
            .catch(e => next(createError(500, e.message)));
    }
}

exports.put = (req, res, next) =>{
    const data = extractValue(req.body, ['guid', 'name']);

    if('guid' in data){
        let name = data['name'].trim();

        if(name.length > 0){
            return  BeehiveModel.updateOne({'guid': data['guid']}, {'name': name})
                .then(() => next(createError(200)))
                .catch(e => next(createError(500)));
        }
    }

    next(createError(400, 'guid is empty or not correct'));
}

/**
  * Push a new measures for the beehive that correspond guid
  * @param req
  * @param res
  */
exports.setMeasures = (req, res, next) => {
    const { guid } = req.beehive;
    const args = ['h', 't', 'w', 'p'];

    const queryValue = extractValue(req.query, args);

    if('h' in queryValue && 't' in queryValue){
        queryValue['h'] = parseInt(queryValue['h']);
        queryValue['t'] = parseInt(queryValue['t']);

        if (queryValue['h'] && queryValue['t']){
            BeehiveModel.updateOne({'guid': guid}, {
                lastHumidity: queryValue['h'],
                lastTemperature: queryValue['t'],
                lastConnexion: Date.now(),
                $push: {
                    measures: new BeehiveArrayModel(
                        {
                            data: new BeehiveMeasure(
                                {
                                    'humidity': queryValue['h'],
                                    'temperature':  queryValue['t'],
                                    'weight': queryValue['w'],
                                    'pressure': queryValue['p'],
                                })
                        })
                },
            }).then(r => next(createError(201)))
                .catch(e => next(createError(300)));
        }else {
            next(createError(400, 'Some arguments are invalid.'));
        }
    } else {
        next(createError(400, 'Please give good arguments !'));
    }
}

exports.getMeasures = (req, res, next) => {
    const { guid } = req.beehive;

    BeehiveModel.findOne({"guid": guid}, {'measures': 1, 'name': 1})
        .then(d => {
            let measures = d['measures'];

            let result = {
                pressure: [],
                weight: [],
                temperature: [],
                humidity: [],
                name: d.name,
            }

            measures.forEach(d => {
                result.pressure.push(d['data'].pressure);
                result.weight.push(d['data'].weight);
                result.temperature.push(d['data'].temperature);
                result.humidity.push(d['data'].humidity);
            })

            // res.status(200).json(measures.map(f => f['data'].toObject()));
            res.status(200).json(result);
        })
        .catch(e => next(createError(500, e.message)));
}