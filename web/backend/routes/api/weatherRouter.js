const express = require('express');
const router = express.Router();

const weatherStationCtrl = require('../../app/controllers/weatherStationController');
const Auth = require('../../app/middleware/authentication');


router.get('/', Auth.needToken, weatherStationCtrl.getData);
router.post('/', Auth.needToken, weatherStationCtrl.createData);
router.get('/measures', Auth.needToken, weatherStationCtrl.getMeasures);
router.post('/measures', Auth.needToken, weatherStationCtrl.setMeasures);

module.exports = router;
