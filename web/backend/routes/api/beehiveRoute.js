const express = require('express');
const router = express.Router();

const ctrlBeehive = require('../../app/controllers/beehiveController');
const Auth = require('../../app/middleware/authentication');

router.get('/', Auth.needToken, ctrlBeehive.get);
router.put('/', Auth.needToken, ctrlBeehive.put);
router.get('/one', Auth.needToken, ctrlBeehive.needGUID, ctrlBeehive.get);
router.get('/measures', Auth.needToken, ctrlBeehive.needGUID, ctrlBeehive.getMeasures);

router.get('/connect', ctrlBeehive.needGUID, ctrlBeehive.connect);
router.put('/client/measures', ctrlBeehive.needGUID, ctrlBeehive.setMeasures)


module.exports = router;