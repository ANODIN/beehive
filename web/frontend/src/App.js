import React, {Component} from "react";
import {Cookies} from "react-cookie";
import BeehiveAPI from "./app/BeehiveAPI";
import {AuthForm} from "./Components/Form/Authentication";
import Dashboard from "./Components/Dashboard/Dashboard";
import {Spinner} from "./Components/Utils";
import Editor from "./Components/Editor";


const NAME_OF_COOKIE_TOKEN = 't';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            menuIndex: '',
            weatherStationData: {}
        }
        this._cookies = new Cookies();
        this._api = new BeehiveAPI(
            'http://localhost:3000',
            this._cookies.get(NAME_OF_COOKIE_TOKEN));
    }

    componentDidMount() {
        this._start();
    }

    /**
     * Get user is  connected, and check if load menu
     * @private
     */
    _start = () => {
        if (this._api.getToken()) {
            this._api.getWeatherStation()
                .then(data => this._onChangePage('ws-view', data))//((data.length > 0) ? 'ws-view' : 'editor')))
                .catch(e => {
                    this._onChangePage('ws-editor')
                });
        }else {
            this._onChangePage('auth')
        }
    }

    _onChangePage = (name, wsItems = undefined) => {
        this.setState({
            menuIndex: name,
            isLoading: false,
            weatherStationData: (wsItems) ? wsItems : {}});
    }

    /**
     * Call the API and try to login (guarantee the bind)
     * @param data
     * @private
     */
    _onLogin = (data) => {
        this.setState({isLoading: true})
        this._api.login(data)
            .then(credential => {
                this._cookies.set(
                    NAME_OF_COOKIE_TOKEN,
                    credential.token);
                this._api.setToken(credential.token);
            })
            .catch(e => {
                console.log('login error,', e.message);
            }).finally(() => {this._start();});
    }

    _onSignup = (data) => {
        this.setState({isLoading: true})
        this._api.signup(data)
            .then(credential => {
                this._cookies.set(
                    NAME_OF_COOKIE_TOKEN,
                    credential.token);
                this._api.setToken(credential.token);
            }).catch(e => {
                console.log('error', e);
        }).finally(() => {this._start();});
    }

    _handleSendConfig = (data) => {
        this.setState({isLoading: true});
        this.setState({weatherStationData: data})
        this._api.setWeatherStationConfiguration(data)
            .then(d => {
                this._onChangePage('ws-view', d)
            })
            .catch(e => {this._onChangePage('ws-editor')});
    }

    _draw() {
        const {isLoading, menuIndex, weatherStationData} = this.state;
        if (isLoading) {
            return (<Spinner center/>);
        } else {
            switch (menuIndex) {
                case 'auth':
                    return (<div className="display-center">
                            <h1>Hello !</h1>
                            <AuthForm
                                data={{email:'example@mail.com', pass:'1234'}}
                                onLogin={this._onLogin}
                                onSignup={this._onSignup}/>
                    </div>);

                case 'ws-editor':
                    return (<div className="display-center">
                        <Editor data={weatherStationData} onSubmit={this._handleSendConfig}/>
                    </div>);

                case 'ws-view':
                    return (<Dashboard api={this._api} data={weatherStationData}/>);

                default:
                    return (<div className="container-fluid bg-dark"><h1>Not found</h1></div>);
            }
        }
    }

    render() {
        return this._draw();
    }
}

export default App;