import {Component} from "react";
import DashboardNavbar from "./DashboardNavbar";
import DashboardContent from "./DashboardContent";
import * as PropTypes from "prop-types";

/**
 * This is the root of the dashboard
 */
class Dashboard extends Component {

    constructor(props) {
        super(props);

        this._api = props.api;
    }

    render (){
        return(<div className="bg-light">
            <DashboardNavbar wsData={this.props.data}/>
            <DashboardContent api={this._api} wsData={this.props.data}/>
        </div>);
    }

}

Dashboard.propTypes = {
    api: PropTypes.object.isRequired,
}

export default Dashboard;