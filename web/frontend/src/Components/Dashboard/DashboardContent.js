import {Bar, Line} from "@reactchartjs/react-chart.js";
import PropTypes from "prop-types";
import {Component} from "react";
import {Spinner} from "../Utils";

/**
 * This components generate the content of dashboard main page
 * @param name
 * @returns {JSX.Element}
 * @constructor
 */
class DashboardContent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            currentBeehiveName: '',
            measures: [],
        }

        this._api = props.api;
        this._defaultChartOptions = {
            scales: {
                yAxes: [{
                    ticks:
                        {
                            beginAtZero: true,
                        }
                }]
            }
        }
    }

    componentDidMount() {
        this._getBeehiveList();
    }

    _getBeehiveList = () => {
        this._api.beehiveList()
            .then(data => {
                if (data[0]) {
                    this._getBeehiveMeasures(data[0].guid);
                }
                this.setState({
                    'beehiveList': data,
                    'currentBeehiveName': (data[0]) ? data[0].name : 'no name'
                });
            }).catch(e => {
            console.log('beehive error', e);
        });
    }

    _getBeehiveMeasures = (guid) => {
        this.setState({'isLoading': true});
        this._api.getBeehiveMeasuresList(guid)
            .then(data => {
                this.setState({
                    'isLoading': false,
                    'measures': data,
                    'currentBeehiveName': (data) ? data.name : 'no name' })
            }).catch(e => {
            console.log('error no data', e);
        });
    }

    _handleChangeBeehive = (e, guid) => {
        e.preventDefault();

        this._getBeehiveMeasures(guid);
    }

    _beehiveRender = () => {
        const {name} = this.props.wsData;
        const {beehiveList, currentBeehiveName, measures} = this.state;

        const dataTempChart = {
            labels: ['7:00', '8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00'],
            datasets: [
                {
                    label: name,
                    borderColor: 'rgb(255,191,0)',
                    backgroundColor: 'rgb(255,191,0)',
                    fill: false,
                    pointRadius: 5,
                    data: measures.temperature
                },

                {
                    label: 'station',
                    borderColor: 'rgb(178,132,190)',
                    backgroundColor: 'rgb(178,132,190)',
                    fill: false,
                    pointRadius: 5,
                    data: [34, 31, 29, 31, 35, 33, 31, 30, 29, 30] //temperatureData
                },
            ],
        };

        const dataHumiChart = {
            labels: ['7:00', '8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00'],
            datasets: [
                {
                    label: 'humidité critique',
                    borderColor: 'rgb(255,105,5)',
                    backgroundColor: 'rgb(255,105,5)',
                    fill: false,
                    pointRadius: 0,
                    data: [90, 90, 90, 90, 90, 90, 90, 90, 90, 90],
                },
                //première courbe
                {
                    label: name,
                    borderColor: 'rgb(178,132,190)',
                    backgroundColor: 'rgb(178,132,190)',
                    fill: false,
                    pointRadius: 5,
                    data: measures.humidity //humidityData,
                },
            ]
        };

        const dataPluvChart = {
            labels: ['7:00', '8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00'],
            datasets: [
                {
                    type: 'line',
                    label: 'température extérieure',
                    borderColor: 'rgb(171,171,171)',
                    fill: false,
                    pointRadius: 5,
                    data: measures.temperature //temperatureData,
                },
                //première courbe
                {
                    label: 'pluie',
                    borderColor: 'rgb(204,238,255)',
                    backgroundColor: 'rgb(204,238,255)',
                    data: measures.humidity //rainfallData
                },
            ]
        };

        const dataSunChart = {
            labels: ['7:00', '8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00'],
            datasets: [
                {
                    label: 'éclairage ambient',
                    borderColor: 'rgb(255,219,0)',
                    backgroundColor: 'rgb(255,219,0)',
                    pointRadius: 5,
                    tension: 0,
                    data: [400, 10000, 23000, 75000, 80000, 120000, 26000, 24000, 12000, 800] // sunshineData
                },
            ]
        };

        const optionTempChart = {
            scales: {
                yAxes: [{
                    ticks:
                        {
                            beginAtZero: true,
                            max: 40
                        }
                }]
            }
        };

        const optionHumiChart = {
            scales: {
                yAxes: [{
                    ticks:
                        {
                            beginAtZero: true,
                            max: 100
                        }
                }]
            }
        };

        return (<div>

            <div className="d-flex justify-content-between">
                <h3>INFORMATIONS GÉNÉRALES :</h3>
                <div>
                    <div className="dropdown">
                        <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            {currentBeehiveName}
                        </button>
                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            {beehiveList.map(({_id, name, guid}) => {
                                return (<li key={_id}><a className="dropdown-item" href={_id}
                                                         onClick={e => this._handleChangeBeehive(e, guid)}>{name}</a>
                                </li>);
                            })}
                        </ul>
                    </div>
                </div>
            </div>

            <hr/>
            <div className="px-2">
                <div className="row">
                    <div className="col-6">
                        <h4>🌡 Température | (°C)</h4>
                        <Line data={dataTempChart} options={optionTempChart}/>
                    </div>

                    <div className="col-6">
                        <h4> 🌫 Humidité | (%)</h4>
                        <Line data={dataHumiChart} options={optionHumiChart}/>
                    </div>
                </div>
                <hr/>
                <div className="row">
                    <div className="col-6">
                        <h4> 🌧 Pluviométrie | (mm) - (%)</h4>
                        <Bar data={dataPluvChart} options={this._defaultChartOptions}/>
                    </div>

                    <div className="col-6">
                        <h4>🌤 Ensoleillement | (Lux)</h4>
                        <Bar data={dataSunChart} options={this._defaultChartOptions}/>
                    </div>
                </div>
            </div>
        </div>);
    }

    render() {
        const {isLoading} = this.state;

        return (<div className="px-3">
            <div className="pt-3">
                <h3>MESURES DE LA STATION MÉTÉO :</h3>
                <hr/>
                <div className="row p-1">
                    <div className="col-6">
                        <div className="card">
                            <div className="card-body">
                                <h5>🌡 Température : 36 °C</h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="card">
                            <div className="card-body">
                                <h5>🌫 Humidité : 45 %</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr/>
            {(isLoading) ? <Spinner center/> : this._beehiveRender()}
        </div>);
    }
}

DashboardContent.propTypes = {
    wsData: PropTypes.object.isRequired,
    api: PropTypes.object.isRequired
}

export default DashboardContent;
