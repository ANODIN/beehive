import Input, {InputType} from "./Form/Form";
import {useState} from "react";
import PropTypes from 'prop-types';

function Editor({data, onSubmit}) {

    const [name, setName] = useState((data) ? data.name || '' : '');
    const [description, setDescription] = useState((data) ? data.desc || '' : '');
    const [lat, setLat] = useState((data && data.coordinate) ? data.coordinate.lat || '' : '');
    const [long, setLong] = useState((data && data.coordinate) ? data.coordinate.long || '' : '');

    const _handleSubmit = e => {
        e.preventDefault();
        onSubmit({
            name: name,
            desc: description,
            coordinate: {
                lat: lat || undefined,
                long: long || undefined
            }
        })
    }

    return (<div className="row">
        <div className="col offset-md-1 col-md-10">
            <div className="text-center">
                <h1>Weather station configuration</h1>
                <hr/>
            </div>

            <form action="post">
                <Input type={InputType.TEXT} id="name" value={name} onChange={setName} required>
                    Name
                </Input>
                <Input type={InputType.TEXT_AREA} id="name" value={description} onChange={setDescription} required>
                    Description
                </Input>

                <div>
                    <hr/>
                    <h3>Position configuration</h3>
                </div>

                <div className="row mb-5">
                    <div className="col">
                        <Input type={InputType.NUMBER} id="name" value={lat}
                               onChange={setLat}/>
                    </div>
                    <div className="col">
                        <Input type={InputType.NUMBER} id="name" value={long}
                               onChange={setLong}/>
                    </div>
                </div>

                <div className="d-grid">
                    <button onClick={_handleSubmit} href="#" className="btn btn-success btn-lg">Create</button>
                </div>
            </form>
        </div>
    </div>);
}

Editor.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    data: PropTypes.object
}

export default Editor;