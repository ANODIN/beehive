function Spinner({small, center = false}) {
    const spinnerCssClass = 'spinner-border text-warning ' + ((small)
        ? 'spinner-border-sm'
        : '');
    return (<div className={(center) ? 'text-center' : ''}>
        <div className={spinnerCssClass} role="status"></div>
    </div>);
}

export {Spinner};