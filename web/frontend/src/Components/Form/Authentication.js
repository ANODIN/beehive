import React, {useState} from 'react';
import Input, {InputType} from "./Form";
import PropTypes from 'prop-types';

const AuthAction = {
    SIGNUP: 0,
    LOGIN: 1
}

/**
 * Genereate a login form
 * @param email
 * @param pass
 * @param onSubmit
 * @returns {JSX.Element}
 * @constructor
 */
function AuthForm({data, onLogin, onSignup}) {

    const [email, setEmail] = useState(((data) ? data.email || '' : ''));
    const [pass, setPass] = useState(((data) ? data.pass || '' : ''));

    const _handleClick = (e, action = AuthAction.LOGIN) => {
        e.preventDefault();

        let data = {
            email: email,
            password: pass
        };

        switch (action){
            case AuthAction.LOGIN:
                onLogin(data);
                break;
            case AuthAction.SIGNUP:
                onSignup(data);
                break;
            default:
                break;
        }
    }

    return (<form method="post" className="p-3">

            <Input type={InputType.EMAIL} id="email" value={email} onChange={setEmail} required>Email</Input>
            <Input type={InputType.PASSWORD} id="pass" value={pass} onChange={setPass} required>Password</Input>

            <div className="m-4 row justify-content-center">
                <div className="col-7 d-grid gap-2">
                    <button type="button" className="btn btn-primary" onClick={e => _handleClick(e)}>Login</button>
                </div>
                <div className="col-5 d-grid gap-2">
                    <button type="button" className="btn btn-outline-warning" onClick={e => _handleClick(e, AuthAction.SIGNUP)}>Signup</button>
                </div>
            </div>
        </form>);
}

AuthForm.propTypes = {
    defaultEmail: PropTypes.string,
    defaultPass: PropTypes.string,
    onLogin: PropTypes.func.isRequired,
    onSignup: PropTypes.func.isRequired,
}

export {AuthForm, AuthAction};