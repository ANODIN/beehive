class BeehiveAPI {
    constructor(host, token = undefined, userId = undefined) {
        this._host = host;
        this._token = token;
        this._userId = userId;
    }

    _getHeader() {
        let myHeaders = new Headers();
        myHeaders.append("Content-type", "application/json");
        if (this._token)
            myHeaders.append("Authorization", "Basic " + this._token);
        return myHeaders;
    }

    _getOptions(myMethod = 'GET', bodyData = undefined) {
        let r = {
            method: myMethod,
            headers: this._getHeader(),
            mode: "cors",
        };
        switch (myMethod){
            case 'GET':
            case 'HEAD':
                return r;
            default:
                return {
                    ...r,
                    body: JSON.stringify(this._setUserId(bodyData || {}))
                };
        }
    }

    _setUserId(data) {
        return (this._userId)
            ? {userId: this._userId, ...data}
            : data;
    }

    _getUrl(path) {
        return this._host + path;
    }

    setToken (token) {
        this._token = token;
    }

    getToken () {
        return this._token;
    }

    async getWeatherStation() {
        const resp = await fetch(
            this._getUrl('/api/ws/'),
            this._getOptions());

        if(!resp.ok)
            throw new Error();

        const json = await resp.json();
        return json;
    }

    async setWeatherStationConfiguration(data) {
        const resp = await fetch(
            this._getUrl('/api/ws/'),
            this._getOptions('POST', data));

        if(!resp.ok)
            throw new Error();

        const json = await resp.json();
        return json;
    }

    async login(data) {
        const resp = await fetch(
            this._getUrl('/api/user/login'),
            this._getOptions('POST', data));

        if(!resp.ok)
            throw new Error();

        const json = await resp.json();
        return json;
    }

    async signup(data) {
        const resp = await fetch(
            this._getUrl('/api/user/signup'),
            this._getOptions('POST', data)
        )

        if(!resp.ok)
            throw new Error();

        const json = await resp.json();
        return json;
    }

    async beehiveList (){
        const resp = await fetch(
            this._getUrl('/api/beehive'),
            this._getOptions()
        )

        if(!resp.ok)
            throw new Error();

        const json = await resp.json();
        return json;
    }

    async getBeehiveMeasuresList (guid){
        const resp = await fetch(
            this._getUrl('/api/beehive/measures?guid=' + guid),
            this._getOptions()
        )

        if(!resp.ok)
            throw new Error();

        const json = await resp.json();
        return json;
    }

}

export default BeehiveAPI;